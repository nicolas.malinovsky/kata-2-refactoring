class Item {
  constructor(name, venderEn, quality) {
    this.name = name;
    this.venderEn = venderEn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items = []) {
    this.items = items;
  }
  updateQuality() {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].name != 'Vino añejo' && this.items[i].name != 'Entrada Recital de Dua Lipa') {
        if (this.items[i].quality > 0) {
          if (this.items[i].name != 'Libro de Clean Code') {
            this.items[i].quality = this.items[i].quality - 1;
          }
        }
      } else {
        if (this.items[i].quality < 50) {
          this.items[i].quality = this.items[i].quality + 1;
          if (this.items[i].name == 'Entrada Recital de Dua Lipa') {
            if (this.items[i].venderEn < 11) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
            if (this.items[i].venderEn < 6) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
          }
        }
      }
      if (this.items[i].name != 'Libro de Clean Code') {
        this.items[i].venderEn = this.items[i].venderEn - 1;
      }
      if (this.items[i].venderEn < 0) {
        if (this.items[i].name != 'Vino añejo') {
          if (this.items[i].name != 'Entrada Recital de Dua Lipa') {
            if (this.items[i].quality > 0) {
              if (this.items[i].name != 'Libro de Clean Code') {
                this.items[i].quality = this.items[i].quality - 1;
              }
            }
          } else {
            this.items[i].quality = this.items[i].quality - this.items[i].quality;
          }
        } else {
          if (this.items[i].quality < 50) {
            this.items[i].quality = this.items[i].quality + 1;
          }
        }
      }
    }

    return this.items;
  }
}

module.exports = {
  Item,
  Shop
}
