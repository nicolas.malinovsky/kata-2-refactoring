const {Shop, Item} = require("../src/almacen");

describe("Almacen", function() {
  it("should foo", function() {
    const almacen = new Shop([new Item("foo", 0, 0)]);
    const items = almacen.updateQuality();
    expect(items[0].name).toBe("fixme");
  });
  it("articulo estandar debe decrementar ambos valores", function() {
    const almacen = new Shop([new Item("art", 5, 10)]);
    const items = almacen.updateQuality();
    expect(items[0].quality).toEqual(9);
    expect(items[0].venderEn).toEqual(4);
  });
  

});
