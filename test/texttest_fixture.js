
const { Shop, Item } = require("../src/almacen");

const items = [
  new Item("Bayaspirina C", 10, 20),
  new Item("Vino añejo", 2, 0),
  new Item("Coca Cola", 5, 7),
  new Item("Libro de Clean Code", 0, 80),
  new Item("Libro de Clean Code", -1, 80),
  new Item("Entrada Recital de Dua Lipa", 15, 20),
  new Item("Entrada Recital de Dua Lipa", 10, 49),
  new Item("Entrada Recital de Dua Lipa", 5, 49),

  // Este no funciona bien
  new Item("Manaos", 3, 6),
];

const days = 2;
const almacen = new Shop(items);


for (let day = 0; day < days; day++) {
  console.log(`\n-------- day ${day} --------`);
  console.log("name, venderEn, quality");
  items.forEach(item => console.log(`${item.name}, ${item.venderEn}, ${item.quality}`));
  almacen.updateQuality();
}
